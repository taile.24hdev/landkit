let myTable = document.querySelector('.users_list');
const pageList = document.querySelector('.page__list');
const pageText = document.querySelector('.page__text');
const pagePrev = document.querySelector('.page__prev');
const pageNext = document.querySelector('.page__next');

let itemOfPage = 10;

let dataItems = dataSet.length;
let startPage = 1;
let endPage = 2;
let indexOfPage = 1;

// All Main Features
window.onload = () => {
    renderPage();
    showData(1);
    sortByProperty(sortItems[0],true,0);
}


const renderPage = (data = dataSet) => {
    startPage = 1;
    endPage = (data.length % itemOfPage > 0) ? Math.floor((data.length / itemOfPage)) + 1 : (data.length / itemOfPage);
    let str = '';
    for (let i = startPage; i <= endPage; i++) {
        str += `<a href="#" onclick="showData(${i});">${i}</a>`;
    }
    pageList.innerHTML = str;
}

const renderRow = (item) => `
    <tr>
        <td>${item[0]}</td>
        <td>${item[1]}</td>
        <td>${item[2]}</td>
        <td>${item[3]}</td>
        <td>${item[4]}</td>
        <td>${item[5]}</td>
    </tr>
`;

const setActiveBtn = () => {
    pagePrev.removeAttribute('disabled');
    pagePrev.classList.remove('button--disabled');
    pageNext.removeAttribute('disabled');
    pageNext.classList.remove('button--disabled');
    if (indexOfPage == startPage) {
        pagePrev.classList.add('button--disabled');
        pagePrev.setAttribute('disabled', 'true');
    }
    if (indexOfPage == endPage) {
        pageNext.classList.add('button--disabled');
        pageNext.setAttribute('disabled', 'true');
    }
}

const setActive = () => {
    for (let i = startPage; i <= endPage; i++) {
        if (i != indexOfPage) {
            pageList.children[i - 1].classList.remove('active');
        }
        pageList.children[indexOfPage - 1].classList.add('active');
    }
}

const plusPage = (index) => {
    showData(indexOfPage += index);
}

const showData = (page,data = dataSet,isSearch = false) => {
    if (page < 1) {
        indexOfPage = 1;
    } else if (page > endPage) {
        indexOfPage = endPage;
    } else {
        indexOfPage = page;
    }
    
    setActiveBtn();
    setActive();
    const startItem = (data.length == 0) ? -1 : (indexOfPage - 1) * itemOfPage;
    const endItem = (startItem + itemOfPage) < data.length ? (startItem + itemOfPage) : data.length;
    pageText.innerHTML = `Showing ${startItem + 1} to ${endItem} of ${data.length} entries`;
    if(isSearch) {
        pageText.innerHTML = `Showing ${startItem + 1} to ${endItem} of ${data.length} entries (filtered from ${dataItems} total entries)`;
    }
    myTable.innerHTML = data.slice(startItem, endItem).reduce((res, x) => res + renderRow(x), '');
}



const selectOption = (select) => {
    itemOfPage = Number(select.value);
    renderPage();
    showData(1);
}

// Reset Disable Sort 
const sortItems = document.querySelectorAll('.th__sort div');
const disabledSort = () => sortItems.forEach((item) => {
    item.classList.remove('th__sort--enabled')
});


// Sort By Property

const sortByProperty = (element,check,index) => {
    disabledSort();
    element.classList.add('th__sort--enabled');
    const startItem = (indexOfPage - 1) * itemOfPage;
    const endItem = (startItem + itemOfPage) < dataItems ? (startItem + itemOfPage) : dataItems;
    let arrAfterSort = [];
    if(check) {
        if(index == 5) {
            arrAfterSort = dataSet.sort((a,b) => (parseInt(a[index].replace(/\D/g, "")) > parseInt(b[index].replace(/\D/g, ""))) ? 1 : -1);
        }else {
            arrAfterSort = dataSet.sort((a,b) =>(a[index] > b[index]) ? 1 : -1);
        }
    }else {
        if(index == 5) {
            arrAfterSort = dataSet.sort((a,b) => (parseInt(a[index].replace(/\D/g, "")) < parseInt(b[index].replace(/\D/g, ""))) ? 1 : -1);
        }else {
            arrAfterSort = dataSet.sort((a,b) =>(a[index] < b[index]) ? 1 : -1);
        }
    }
    myTable.innerHTML = arrAfterSort.slice(startItem, endItem).reduce((res, x) => res + renderRow(x), '');
}


// Search By Name
const inputSearch = document.querySelector('.table__action__search input');
inputSearch.addEventListener('input',() => {
    let userList = dataSet.filter(item => item[0].toLowerCase().includes(inputSearch.value.toLowerCase()));
    renderPage(userList);
    showData(1,userList,true);
})




