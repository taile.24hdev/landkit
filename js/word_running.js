const textRunning = document.querySelector('.container__about__contact .text--success .text-running');

const textCursor = document.querySelector(".container__about__contact .text--success .text-cursor");
const allTextRunning = ["developers.", "founders.", "designers."];
var textNumber = 0;
var textIndex = 0;
var intervalVar;
function writingText() {
  var text = allTextRunning[textNumber].slice(0, textIndex + 1);
  textRunning.innerHTML = text;
  textIndex++;
  if (text === allTextRunning[textNumber]) {
    clearInterval(intervalVar);

    var cursorInterval = setInterval(() => {
      if (textCursor.style.display === "none")
        textCursor.style.display = "inline-block";
      else textCursor.style.display = "none";
    }, 250);

    setTimeout(() => {
      clearInterval(cursorInterval);
      textCursor.style.display = "inline-block";
      intervalVar = setInterval(deletingText, 50);
    }, 1000);
  }
}

function deletingText() {
  var text = allTextRunning[textNumber].slice(0, textIndex - 1);
  textRunning.innerHTML = text;
  textIndex--;

  if (text === "") {
    clearInterval(intervalVar);
    if (textNumber == allTextRunning.length - 1) textNumber = 0;
    else textNumber++;
    textIndex = 0;

    setTimeout(() => {
      intervalVar = setInterval(writingText, 50);
    }, 200);
  }
}

intervalVar = setInterval(writingText, 50);

