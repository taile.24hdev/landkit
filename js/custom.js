const navbarToggleBtn = document.querySelector('.navbar__container__toggler');
const navbarContainerList = document.querySelector('.navbar__container-list');
const navCloseBtn = document.querySelector('.nav__close__btn');
const htmlTag = document.querySelector('html');
const navAccountLi = document.querySelectorAll('.nav__account li');
const cardInputName = document.querySelector('.card__body-form--input #cardName');
const cardInputEmail = document.querySelector('.card__body-form--input #cardEmail');
const cardInputPassword = document.querySelector('.card__body-form--input #cardPassword');
const cardBtn = document.querySelector('.card__body-form-btn');
const cardError = document.querySelectorAll('.card__body-form--input p');

const ToggleNavBarTablet = () => {
    navbarContainerList.classList.toggle('navbar__container-list--show');
    htmlTag.classList.toggle('overflow-hidden');
}

navbarToggleBtn.addEventListener('click', ToggleNavBarTablet);
navCloseBtn.addEventListener('click', ToggleNavBarTablet);


const clearActionAccount = (element) => {
    for (let item of navAccountLi) {
        if (item !== element) {
            const isShowNavSub = item.querySelector('.nav__account__sub--tablet');
            if (isShowNavSub) {
                hideSubNav(item);
            }
        }
    }
}

const hideSubNav = (element) => {
    const subNav = element.querySelector(".nav__account__sub");
    const titleTagI = element.querySelector(".nav__account__title .fa-angle-down");
    titleTagI.classList.remove("fa-angle-up");
    subNav.classList.remove("nav__account__sub--tablet");
}

const showSubNav = (element) => {
    clearActionAccount(element);
    const subNav = element.querySelector(".nav__account__sub");
    const titleTagI = element.querySelector(".nav__account__title .fa-angle-down");
    titleTagI.classList.toggle("fa-angle-up");
    subNav.classList.toggle("nav__account__sub--tablet");
}

// Valid Crendentials

const isValidName = (name) => name !== '';

const isValidEmail = (email) => {
    let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return regex.test(email);
}

const isValidPassword = (password) => password.length >= 8;

const showError = (element, err) => {
    element.parentNode.querySelector('p').style.display = "block";
    element.parentNode.querySelector('p').innerText = err;
}

const clearError = () => {
    cardError.forEach((e) => {
        e.innerHTML = ''
    });
}

const validateInfor = () => {
    clearError();
    let check = true;
    if (!isValidName(cardInputName.value)) {
        showError(cardInputName, 'Invalid name');
        check = false;
    }
    if (!isValidEmail(cardInputEmail.value)) {
        showError(cardInputEmail, 'Invalid email');
        check = false;
    }
    if (!isValidPassword(cardInputPassword.value)) {
        showError(cardInputPassword, 'Invalid password');
        check = false;
    }
    return check;
}

cardBtn.addEventListener('click', () => {
    const check = validateInfor();
    if (check) {
        console.log({
            "Name": cardInputName.value,
            "Email": cardInputEmail.value,
            "Password": cardInputPassword.value
        });
    }
});

// Key down event ENTER
cardInputName.addEventListener('keydown', (event) => {
    if (event.key === 'Enter') {
        clearError();
        if (!isValidName(cardInputName.value)) {
            showError(cardInputName, 'Invalid name');
        }else {
            console.log({Name : cardInputName.value});
        }
    }
});

cardInputEmail.addEventListener('keydown', (event) => {
    if (event.key === 'Enter') {
        clearError();
        if (!isValidEmail(cardInputEmail.value)) {
            showError(cardInputEmail, 'Invalid email');
        }else {
            console.log({Email: cardInputEmail.value});
        }
    }
});

cardInputPassword.addEventListener('keydown', (event) => {
    if (event.key === 'Enter') {
        clearError();
        if (!isValidPassword(cardInputPassword.value)) {
            showError(cardInputPassword, 'Invalid password');
        }else {
            console.log({Password : cardInputPassword.value});
        }
    }
});



