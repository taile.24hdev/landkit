const sliderCustomer = document.querySelectorAll('.container__customer__content');

let slideIndex = 1;
showSlides();

const setSlideIndex = (n) => {
    if(n > sliderCustomer.length) {
        slideIndex = 1;
    }
    if(n < 1) {
        slideIndex = sliderCustomer.length;
    }
}

const plusSlides = (n) => {
    setSlideIndex(slideIndex += n);
    const animationName = (n==1) ? 'fadeContentNext' : 'fadeContentPrevious';
    sliderCustomer[slideIndex-1]
        .querySelector('.container__customer__content-desc-logo')
        .style['animation-name'] = animationName;
    showSlides();
}

function showSlides() {
    for(let i = 0; i < sliderCustomer.length; i++) {
        sliderCustomer[i].style.display = "none";
    }
    sliderCustomer[slideIndex-1].style.display = "flex";
}
